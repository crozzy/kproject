# Kproject #

This is the meta project housing:

* [php-server](https://bitbucket.org/crozzy/php-server)
* [node-consumer](https://bitbucket.org/crozzy/node-consumer)

### How do I deploy this? ###

(spoiler: with docker)

* Build the images:
```
docker pull redis
docker build -t node-consumer:latest --rm node-consumer/
docker build -t php-server:latest --rm php-server/
```

* Let supervisor do the rest
```
apt-get install supervisor
cp krproject/supervisor-consumer.conf /etc/supervisor/conf.d/
supervisorctl reread
supervisorctl update
supervisorctl restart all
```

### Log analysis ###

* Install bunyan in host and examine `/www-data-log/consumer.log`.

    * npm has some nice tools to use with bunyan, this is an example of using daggr and json packages to produce a power-of-two histogram showing the distribution of the response time:
```sh
root@Candidate:/opt# cat /www-data-log/consumer.log | bunyan -0 --strict | json -ga respTime | daggr quantize


           value  ------------- Distribution ------------- count
               4 |                                         0
               8 |@                                        59
              16 |@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@           1522
              32 |@@@@@@@@                                 405
              64 |@                                        37
             128 |@                                        29
             256 |                                         0
             512 |                                         0
            1024 |                                         0
            2048 |                                         0
            4096 |                                         3
            8192 |                                         0


```

* Future advance: Install [graylog](https://www.graylog.org/) in host. (I tried this but ran out of mem, should probably be hosted on a different box)

### jMeter testing ###

* To test the server's response time I used jMeter, given that we are simply communicating a simple message back to the user we expect this to be fast. As you can see from the [graph](https://bytebucket.org/crozzy/kproject/raw/ce23a02744f291c40bbd20ccda19847d41bdbd59/graph_results.png) it is when dealing with 50 users/second.

* This metric in itself isn't so useful without analyzing the consumer log (above).

### Scaling ###

* Using docker we can easily scale any module in this project (php-server, node-consumer), if we find any bottle necks.
* BRPOP will ensure that every queue item is consumed once, so we won't get duplicates.

### NGINX config ###

* By default nginx will allow 1024 concurrent connections, as we kept the php server purposefully light, we can expect to handle 1024 connection/second.

* On a multi-core machine we can quite safely  increase `worker_processes=<#cores>` (concurrent connections = worker_processes*worker_connections). We can also increase `worker_connections` if we are sure that we have no heavily i/o bound functionality and enough system RAM.